# Weather APP

* [Setup](#setup)
* [Architecture](#architecture)
* [Libraries](#libraries)
* [Features](#features)
* [Testing](#testing)
* [Unimplemented features](#todo)


## <a name="setup"></a> Setup 👣
OpenWeatherAPI key in local.properties with name
API_KEY="string"

Running the application on emulator requires mock location. 
Open emulator device and setup a location, after that open Google maps to see if it's working.

## <a name="architecture"></a> Architecture 👣
MVVM + Clean App + Coroutines + Flow

## <a name="libraries"></a> Libraries 🧰
* Koin
* Retrofit
* Compose & Compose previews
* Location services
* Navigation
* Mockito
* Room


## <a name="features"></a> Implemented features 🎨
- Permission flow for location & getting device location
- Getting the forecast data for 5 days, data manipulation can be found in UserDataRepository
- Adding the location to favorites
- Reading and updating the favorites list


## <a name="testing"></a> Testing 👮
Unit tests for each data layers can be found


### <a name="todo"></a> Unimplemented features 🤝
- The ability to get extra information about a specific location Using something like the Google Places API https://developers.google.com/places/web-
   service/intro
- The ability to see weather information while the application is offline (show time last
  updated etc) - this could be done using room to save the last displayed data
- Allow the user to view saved locations on a map - google maps multiple pins setup
