package com.example.domain

import com.example.domain.model.WeatherData
import com.example.domain.repository.Repository
import com.example.domain.usecase.GetDataUseCase
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.test.assertEquals

class GetDataUseCaseTest {
    private val mockRepo = mock<Repository>()
    private val getDataUseCase = GetDataUseCase(mockRepo)

    @Test
    fun getDataUseCase_emptyData() = runTest {
        val result = listOf<WeatherData>()
        val lat = 1.0
        val long = 2.0
        whenever(mockRepo.getData(lat, long)) doReturn flow { emit(result) }
        val test = getDataUseCase.invoke(lat, long).first()

        assertEquals(test, result)
    }
}
