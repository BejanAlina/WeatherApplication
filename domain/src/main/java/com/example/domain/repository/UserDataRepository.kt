package com.example.domain.repository

import com.example.domain.model.FavoriteData
import com.example.domain.model.WeatherData
import kotlinx.coroutines.flow.Flow


interface Repository {

    suspend fun addFavoriteLocation(latitude: Double, longitude: Double)
    suspend fun removeFromFavorites(latitude: Double, longitude: Double)

    fun getData(latitude: Double, longitude: Double): Flow<List<WeatherData>>

    fun getFavoritesData(): Flow<List<FavoriteData>>

    fun isInFavorite(latitude: Double, longitude: Double): Flow<Boolean>
}