package com.example.domain.model

data class FavoriteData(
    val latitude: Double,
    val longitude: Double
)
