package com.example.domain.model

import java.util.Date


data class WeatherData(
    val weatherState: WeatherState,
    val minDegrees: Double,
    val maxDegrees: Double,
    val averageDegrees: Double,
    val date: Date?
)


enum class WeatherState(private val value: String) {
    RAIN("Rain"),
    SUNNY("Clear"),
    CLOUDY("Clouds"),
    UNKNOWN("");

    companion object {
        fun fromString(value: String) = entries.find { it.value.equals(value, ignoreCase = true) } ?: UNKNOWN
    }
}