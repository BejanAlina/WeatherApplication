package com.example.domain.usecase

import android.annotation.SuppressLint
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine

class GetLocationUseCase(
    private val fusedLocationProviderClient: FusedLocationProviderClient
) : LocationTracker {
    @SuppressLint("MissingPermission")
    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getCurrentLocation(): Pair<Double, Double>? {
        return suspendCancellableCoroutine { cont ->
            fusedLocationProviderClient.lastLocation.apply {
                if (isComplete) {
                    if (isSuccessful) {
                        cont.resume(
                            Pair(
                                result.latitude,
                                result.longitude
                            )
                        ) {}
                    } else {
                        cont.resume(null) {}
                    }
                    return@suspendCancellableCoroutine
                }
                addOnSuccessListener {
                    if (it != null) {
                        cont.resume(
                            Pair(
                                it.latitude,
                                it.longitude
                            )
                        ) {}
                    } else {
                        cont.resume(null) {}
                    }
                }
                addOnFailureListener {
                    cont.resume(null) {}
                }
                addOnCanceledListener {
                    cont.cancel()
                }
            }
        }
    }
}

interface LocationTracker {
    suspend fun getCurrentLocation(): Pair<Double, Double>?
}
