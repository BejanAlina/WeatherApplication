package com.example.domain.usecase

import com.example.domain.repository.Repository

class AddToFavoritesUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(lat: Double, long: Double) {
        println("add location to favorites called lat $lat long $long")
        repository.addFavoriteLocation(lat, long)
    }
}