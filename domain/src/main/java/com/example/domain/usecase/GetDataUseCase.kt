package com.example.domain.usecase

import com.example.domain.model.WeatherData
import com.example.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class GetDataUseCase(
    private val repository: Repository
) {
    operator fun invoke(lat: Double, long: Double): Flow<List<WeatherData>> {
        return repository.getData(lat, long)
    }
}