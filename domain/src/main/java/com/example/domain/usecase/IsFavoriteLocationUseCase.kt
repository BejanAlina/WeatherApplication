package com.example.domain.usecase

import com.example.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class IsFavoriteLocationUseCase(
    private val repository: Repository
) {
    operator fun invoke(lat: Double, long: Double): Flow<Boolean> {
        println("is location favorite use case called lat $lat and long $long")
        return repository.isInFavorite(lat, long)
    }
}