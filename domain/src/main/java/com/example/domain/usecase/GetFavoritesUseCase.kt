package com.example.domain.usecase

import com.example.domain.model.FavoriteData
import com.example.domain.repository.Repository
import kotlinx.coroutines.flow.Flow

class GetFavoritesUseCase(
    private val repository: Repository
) {
    operator fun invoke(): Flow<List<FavoriteData>> {
        return repository.getFavoritesData()
    }
}