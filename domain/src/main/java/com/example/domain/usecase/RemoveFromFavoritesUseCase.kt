package com.example.domain.usecase

import com.example.domain.repository.Repository

class RemoveFromFavoritesUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(latitude: Double, longitude: Double) {
        repository.removeFromFavorites(latitude, longitude)
    }
}