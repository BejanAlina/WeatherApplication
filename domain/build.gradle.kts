plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.example.domain"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation("com.google.android.gms:play-services-location:${Versions.GMS}")
    implementation("com.google.accompanist:accompanist-permissions:${Versions.ACMP}")
    implementation("androidx.core:core-ktx:${Versions.KTX}")
    implementation("androidx.appcompat:appcompat:${Versions.COMPAT}")

    testImplementation("org.robolectric:robolectric:${Versions.ROBOELECTRIC}")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.MOCK}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.COROUTINE_TEST}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.COROUTINE_TEST}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINE_TEST}")
    testImplementation(kotlin("test"))

}