plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
}

android {
    namespace = "com.example.data"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    api("com.squareup.retrofit2:retrofit:${Versions.RETROFIT}")
    api("com.squareup.retrofit2:converter-gson:${Versions.RETROFIT}")
    api("com.squareup.okhttp3:okhttp:${Versions.OK_HTTP}")
    api("com.squareup.okhttp3:logging-interceptor:${Versions.OK_HTTP}")
    api("androidx.room:room-runtime:${Versions.ROOM}")
    kapt("androidx.room:room-compiler:${Versions.ROOM}")
    api("androidx.room:room-ktx:${Versions.ROOM}")

    //domain
    implementation(project(":domain"))

    testImplementation("org.robolectric:robolectric:${Versions.ROBOELECTRIC}")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.MOCK}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.COROUTINE_TEST}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINE_TEST}")
    testImplementation(kotlin("test"))
}