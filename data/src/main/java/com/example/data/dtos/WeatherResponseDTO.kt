package com.example.data.dtos

import com.google.gson.annotations.SerializedName

data class WeatherResponseDTO(
    @SerializedName("list")
    val list: List<WeatherListDTO>,
)

data class WeatherListDTO(
    @SerializedName("main")
    val main: MainDTO,
    @SerializedName("weather")
    val weather: List<WeatherDTO>,
    @SerializedName("dt_txt")
    val dateText: String
)

data class MainDTO(
    @SerializedName("temp_min")
    val temp_min: Double,
    @SerializedName("temp_max")
    val temp_max: Double,
    @SerializedName("temp")
    val temp: Double,
)
data class WeatherDTO(
    @SerializedName("main")
    val main: String,
)