package com.example.data.remote

import com.example.data.dtos.WeatherResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {
    @GET("/data/2.5/forecast")
    suspend fun getWeather(
        @Query("lat") lat: Double? = null,
        @Query("lon") long: Double? = null
    ): Response<WeatherResponseDTO>
}