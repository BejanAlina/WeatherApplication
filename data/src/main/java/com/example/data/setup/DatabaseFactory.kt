package com.example.data.setup

import android.content.Context
import androidx.room.Room
import com.example.data.local.database.AppDatabase
import com.example.data.local.database.DatabaseConstants.DATABASE_NAME

class DatabaseFactory {
    fun getOrCreateDatabaseInstance(appContext: Context): AppDatabase {
        return Room.databaseBuilder(appContext, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

}

