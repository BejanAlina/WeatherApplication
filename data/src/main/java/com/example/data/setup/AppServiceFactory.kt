package com.example.data.setup

import com.example.data.remote.AppService
import okhttp3.Interceptor


class AppServiceFactory(
    private val httpClientFactory: HttpClientFactory,
    private val apiKey: String
) {

    fun getAppService(serviceFactory: ServiceFactory): AppService {
        val httpClient = httpClientFactory.abstractClient.newBuilder()
            .addInterceptor(NetworkModule.basicHeaderInterceptor())
            .addInterceptor(Interceptor { chain ->
                val request = chain.request().newBuilder()
                val originalHttpUrl = chain.request().url
                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("appid", apiKey).build()
                request.url(url)
                chain.proceed(request.build())
            })
            .build()

        val appService = serviceFactory.retrofitInstance.newBuilder().client(httpClient).build()
        return appService.create(AppService::class.java)
    }

}

