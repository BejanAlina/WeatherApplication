package com.example.data.setup

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class HttpClientFactory(debug: Boolean) {
    val abstractClient: OkHttpClient by lazy {
        val builder = OkHttpClient.Builder()
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
        if (debug)
            builder.addNetworkInterceptor(NetworkModule.createHttpLoggingInterceptor())
        builder.build()
    }

}