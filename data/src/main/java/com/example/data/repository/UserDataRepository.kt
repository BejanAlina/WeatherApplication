package com.example.data.repository

import com.example.data.dtos.WeatherResponseDTO
import com.example.data.local.LocalDataSource
import com.example.data.local.model.FavoriteItem
import com.example.data.remote.RemoteDataSource
import com.example.domain.model.FavoriteData
import com.example.domain.model.WeatherData
import com.example.domain.model.WeatherState
import com.example.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import java.text.SimpleDateFormat
import kotlin.math.floor


class UserDataRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : Repository {

    override suspend fun addFavoriteLocation(latitude: Double, longitude: Double) {
        localDataSource.addFavoriteLocation(
            FavoriteItem(
                latitude = latitude,
                longitude = longitude
            )
        )
    }

    override suspend fun removeFromFavorites(latitude: Double, longitude: Double) {
        localDataSource.deleteFavoriteLocation(latitude, longitude)
    }

    override fun getData(latitude: Double, longitude: Double): Flow<List<WeatherData>> {
        return flow {
            emit(
                remoteDataSource.appService.getWeather(latitude, longitude).body()?.toData(sdf)
                    ?: emptyList()
            )
        }
    }

    override fun getFavoritesData(): Flow<List<FavoriteData>> {
        return localDataSource.getFavorites()
            .map { result -> result.map { FavoriteData(it.latitude, it.longitude) } }
    }

    override fun isInFavorite(latitude: Double, longitude: Double): Flow<Boolean> {
        return localDataSource.isLocationFavorite(latitude, longitude)
            .map { value: Int? -> value != null }
    }
}

fun WeatherResponseDTO.toData(formatter: SimpleDateFormat): List<WeatherData> {
    return this.list.asSequence().map {
        WeatherData(
            WeatherState.fromString(it.weather.firstOrNull()?.main ?: ""),
            it.main.temp_min,
            it.main.temp_max,
            it.main.temp,
            formatter.parse(it.dateText)
        )
    }.filter { it.date != null }.groupBy { it.date }.map {
        val max = it.value.maxBy { it.maxDegrees }.maxDegrees
        val min = it.value.maxBy { it.minDegrees }.minDegrees
        val numbersByElement = it.value.groupBy { it.weatherState }.map {
            Pair(it.value.count(), it.key)
        }.maxBy { it.first }

        val average = (max + min) / 2
        WeatherData(
            numbersByElement.second,
            min.kelvinToCelsius(),
            max.kelvinToCelsius(),
            average.kelvinToCelsius(),
            it.value.first().date
        )
    }.toList()
}

fun Double.kelvinToCelsius(): Double {
    return floor(this - 273.15)
}