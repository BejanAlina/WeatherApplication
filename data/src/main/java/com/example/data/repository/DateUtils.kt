package com.example.data.repository

import java.text.SimpleDateFormat
import java.util.Locale

val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)