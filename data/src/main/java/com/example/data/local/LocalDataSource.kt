package com.example.data.local

import com.example.data.local.database.AppDatabase
import com.example.data.local.model.FavoriteItem
import kotlinx.coroutines.flow.Flow

class LocalDataSource(private val database: AppDatabase) {

    suspend fun addFavoriteLocation(favoriteItem: FavoriteItem) {
        database.databaseDao().addFavoriteLocation(favoriteItem)
    }

    suspend fun deleteFavoriteLocation(latitude: Double, longitude: Double) {
        database.databaseDao().deleteFavoriteLocation(latitude, longitude)
    }

    fun isLocationFavorite(latitude: Double, longitude: Double): Flow<Int> {
        return database.databaseDao().isInFavorite(latitude, longitude)
    }

    fun getFavorites(): Flow<List<FavoriteItem>> {
        return database.databaseDao().getData()
    }
}