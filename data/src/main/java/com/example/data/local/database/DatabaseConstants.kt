package com.example.data.local.database

object DatabaseConstants{
    const val DATABASE_NAME = "weather_favorites"
    const val FAVORITES_TABLE = "favorites"
}