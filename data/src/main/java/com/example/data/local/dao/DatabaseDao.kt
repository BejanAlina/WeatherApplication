package com.example.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.example.data.local.model.FavoriteItem
import kotlinx.coroutines.flow.Flow

@Dao
interface DatabaseDao {
    @Upsert
    suspend fun addFavoriteLocation(favoriteItem: FavoriteItem)

    @Query("DELETE FROM favorites WHERE latitude = :latitude AND longitude = :longitude")
    suspend fun deleteFavoriteLocation(latitude: Double, longitude: Double)

    @Query(value = "SELECT * FROM favorites")
    fun getData(): Flow<List<FavoriteItem>>

    @Query("SELECT id FROM favorites WHERE latitude = :latitude AND longitude = :longitude")
    fun isInFavorite(latitude: Double, longitude: Double): Flow<Int>
}