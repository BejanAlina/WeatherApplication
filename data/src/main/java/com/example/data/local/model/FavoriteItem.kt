package com.example.data.local.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.example.data.local.database.DatabaseConstants

@androidx.room.Entity(tableName = DatabaseConstants.FAVORITES_TABLE)
data class FavoriteItem (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "latitude")
    val latitude: Double,
    @ColumnInfo(name = "longitude")
    val longitude: Double,
)
