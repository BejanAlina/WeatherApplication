package com.example.data

import com.example.data.dtos.MainDTO
import com.example.data.dtos.WeatherDTO
import com.example.data.dtos.WeatherListDTO
import com.example.data.dtos.WeatherResponseDTO
import com.example.data.local.LocalDataSource
import com.example.data.local.database.AppDatabase
import com.example.data.remote.AppService
import com.example.data.remote.RemoteDataSource
import com.example.data.repository.UserDataRepository
import com.example.data.repository.sdf
import com.example.domain.model.WeatherData
import com.example.domain.model.WeatherState
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Test
import retrofit2.Response
import kotlin.test.assertEquals

class GetDataUseCaseTest {
    private val mockAppService = mock<AppService>()
    private val mockDataBase = mock<AppDatabase>()
    private val localDataSource = LocalDataSource(mockDataBase)
    private val remoteDataSource = RemoteDataSource(mockAppService)
    private val repository = UserDataRepository(remoteDataSource, localDataSource)

    @Test
    fun averageOfWeatherState() = runTest {
        val lat = 1.0
        val long = 2.0
        val list = listOf(
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:40:00"
            )
        )
        whenever(mockAppService.getWeather(lat, long)) doReturn Response.success(
            WeatherResponseDTO(
                list
            )
        )
        val test = repository.getData(lat, long).first()

        assertEquals(WeatherState.RAIN, test[0].weatherState)
    }

    @Test
    fun averageOfMinMaxTemperature() = runTest {
        val lat = 1.0
        val long = 2.0
        val list = listOf(
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:40:00"
            )
        )
        whenever(mockAppService.getWeather(lat, long)) doReturn Response.success(
            WeatherResponseDTO(
                list
            )
        )
        val test = repository.getData(lat, long).first()

        assertEquals(27.0, test[0].minDegrees)
        assertEquals(66.0, test[0].maxDegrees)
        assertEquals(47.0, test[0].averageDegrees)
    }

    @Test
    fun averageOfMinMaxTemperature_2days_forecast() = runTest {
        val lat = 1.0
        val long = 2.0
        val list = listOf(
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-21 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:40:00"
            ),
            WeatherListDTO(
                main = MainDTO(300.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-21 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(220.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(220.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(220.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(220.4, 340.0, 320.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-21 12:40:00"
            )
        )
        whenever(mockAppService.getWeather(lat, long)) doReturn Response.success(
            WeatherResponseDTO(
                list
            )
        )
        val test = repository.getData(lat, long).first()

        val expectedResult =
            listOf(
                WeatherData(
                    WeatherState.fromString("Rain"),
                    27.0,
                    66.0,
                    47.0,
                    sdf.parse("2024-02-20 12:00:00")
                ),
                WeatherData(
                    WeatherState.fromString("Clouds"),
                    27.0,
                    66.0,
                    47.0,
                    sdf.parse("2024-02-21 12:40:00")
                ),
            )
        assertEquals(expectedResult, test)
    }
}
