package com.example.composeflowcleanapp

import android.app.Application
import com.example.composeflowcleanapp.di.domainModule
import com.example.composeflowcleanapp.di.localModule
import com.example.composeflowcleanapp.di.remoteModule
import com.example.composeflowcleanapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(localModule, remoteModule, domainModule,viewModelModule)
        }
    }
}