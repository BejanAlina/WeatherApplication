package com.example.composeflowcleanapp.presentation.screens.main

import CurrentLocationScreen
import LocationDeniedScreen
import PermissionState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawing
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.windowInsetsTopHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.AbsoluteAlignment
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.composeflowcleanapp.R
import com.example.composeflowcleanapp.presentation.screens.ResultState
import com.example.domain.model.WeatherData
import com.example.domain.model.WeatherState
import org.koin.androidx.compose.koinViewModel
import java.text.SimpleDateFormat
import java.util.Locale


@Composable
fun MainScreen(
    modifier: Modifier = Modifier,
    mainViewModel: MainViewModel = koinViewModel()
) {
    var locationGranted: PermissionState by remember { mutableStateOf(PermissionState.Loading) }
    val uiState by mainViewModel.uiState.collectAsStateWithLifecycle()
    val isFavorite by mainViewModel.isInFavorite.collectAsStateWithLifecycle()

    when (locationGranted) {
        PermissionState.Loading -> {
            CurrentLocationScreen({
                locationGranted = PermissionState.Success
                mainViewModel.getCurrentLocation()
            }, {
                locationGranted = PermissionState.Error
            })
        }

        PermissionState.Success -> {
            MainScreen(
                modifier = modifier,
                uiState = uiState,
                isFavorite = isFavorite,
                onAddToFavoriteClicked = mainViewModel::addLocationToFavorites,
                onRemoveFromFavoritesClicked = mainViewModel::removeLocationFromFavorites
            )
        }

        PermissionState.Error -> {
            LocationDeniedScreen()
        }

        else -> {}
    }
}

@Composable
internal fun MainScreen(
    modifier: Modifier = Modifier,
    uiState: ResultState,
    isFavorite: Boolean?,
    onAddToFavoriteClicked: () -> Unit = {},
    onRemoveFromFavoritesClicked: () -> Unit = {}
) {
    Box(
        modifier = modifier
            .background(color = Color.Black)
            .fillMaxSize()
    ) {
        Spacer(Modifier.windowInsetsTopHeight(WindowInsets.safeDrawing))
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            when (uiState) {
                ResultState.Loading -> {
                    CircularProgressIndicator(
                        modifier = Modifier.align(Alignment.Center)
                    )
                }

                ResultState.LoadFailed -> {
                    Text(
                        stringResource(id = R.string.location_error),
                        modifier = Modifier.align(Alignment.Center)
                    )
                }

                else -> {
                    if (uiState is ResultState.Success) {
                        CurrentWeatherResultBody(
                            uiState.data
                        )
                        isFavorite?.let {
                            FavoriteButton(
                                modifier = Modifier
                                    .align(Alignment.BottomEnd)
                                    .padding(bottom = 60.dp, end = 20.dp), isFavorite,
                                onAddToFavoriteClicked, onRemoveFromFavoritesClicked
                            )
                        }
                    }
                }
            }
        }

    }
}

@Composable
private fun CurrentWeatherResultBody(
    data: List<WeatherData>,
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black),
    ) {

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    colorResource(
                        id =
                        when (data.getOrNull(0)?.weatherState) {
                            WeatherState.RAIN -> R.color.rainy
                            WeatherState.CLOUDY -> R.color.cloudy
                            WeatherState.SUNNY -> R.color.sunny
                            else -> R.color.white
                        }
                    )
                ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            itemsIndexed(data) { index, item ->
                if (index == 0) {
                    CurrentWeatherImage(data = item)
                    CurrentWeatherCard(data = item)
                    Spacer(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(1.dp)
                            .background(Color.White)
                    )
                } else {
                    NextWeatherCurrentCard(data = item)
                }
            }
        }
    }
}

@Composable
fun FavoriteButton(
    modifier: Modifier,
    isFavorite: Boolean,
    onAddToFavoriteClicked: () -> Unit,
    onRemoveFromFavoritesClicked: () -> Unit
) {
    var favBtnClicked by remember {
        mutableStateOf(isFavorite)
    }

    FloatingActionButton(
        modifier = modifier,
        onClick = {
            favBtnClicked = !favBtnClicked
            if (favBtnClicked) onAddToFavoriteClicked.invoke() else onRemoveFromFavoritesClicked.invoke()

        },
    ) {
        Icon(
            if (favBtnClicked) Icons.Filled.Favorite else Icons.Filled.FavoriteBorder,
            "Add/remove favorite.",
            tint = Color.Red
        )
    }
}

@Composable
fun CurrentWeatherImage(data: WeatherData) {
    println("weather stater " + data.averageDegrees)
    Box(
        contentAlignment = Alignment.Center, modifier = Modifier
            .fillMaxHeight()
            .height(350.dp)
    ) {

        Image(
            painterResource(
                when (data.weatherState) {
                    WeatherState.RAIN -> R.drawable.sea_rainy
                    WeatherState.CLOUDY -> R.drawable.sea_cloudy
                    WeatherState.SUNNY -> R.drawable.sea_sunnypng
                    else -> R.drawable.ic_launcher_background // TODO add placeholder image
                }
            ), contentDescription = "current weather",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .fillMaxSize()

        )

        Column(horizontalAlignment = Alignment.CenterHorizontally) {

            Text(
                text = AnnotatedString(
                    text = stringResource(
                        id = R.string.celsius,
                        data.averageDegrees.toInt().toString()
                    )
                ),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Center,
                fontSize = 42.sp,
                fontWeight = FontWeight.Bold,
            )
            Text(
                text = AnnotatedString(
                    text = data.weatherState.name
                ),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Center,
                fontSize = 42.sp
            )
        }
    }
}

@Composable
fun CurrentWeatherCard(data: WeatherData) {
    Box(
        modifier = Modifier
            .padding(12.dp)
            .fillMaxWidth(),
    ) {
        CurrentWeatherCardRow(
            modifier = Modifier
                .align(AbsoluteAlignment.CenterLeft),
            data.minDegrees.toInt().toString(),
            stringResource(id = R.string.min)
        )

        CurrentWeatherCardRow(
            modifier = Modifier
                .align(Alignment.Center),
            data.averageDegrees.toInt().toString(),
            stringResource(id = R.string.now)
        )


        CurrentWeatherCardRow(
            modifier = Modifier
                .align(AbsoluteAlignment.CenterRight),
            data.maxDegrees.toInt().toString(),
            stringResource(id = R.string.max)
        )
    }
}

@Composable
fun CurrentWeatherCardRow(modifier: Modifier, data: String, subtext: String) {
    Column(
        modifier = modifier
    ) {
        Text(
            text = AnnotatedString(
                text = stringResource(
                    id = R.string.celsius,
                    data
                )
            ),
            style = MaterialTheme.typography.bodyLarge,
            textAlign = TextAlign.Center
        )
        Text(text = subtext)
    }
}

@Composable
fun NextWeatherCurrentCard(data: WeatherData) {
    Box(
        modifier = Modifier
            .padding(12.dp)
            .fillMaxWidth(),
    ) {
        Column(
            modifier = Modifier
                .align(AbsoluteAlignment.CenterLeft)
        ) {

            Text(
                text = AnnotatedString(
                    text = SimpleDateFormat("EE", Locale.ENGLISH).format(data.date?.time ?: "")
                ),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Center
            )

        }
        Column(
            modifier = Modifier
                .align(Alignment.Center)
        ) {
            Image(
                painterResource(
                    when (data.weatherState) {
                        WeatherState.RAIN -> R.drawable.rain
                        WeatherState.CLOUDY -> R.drawable.partlysunny
                        WeatherState.SUNNY -> R.drawable.clear
                        else -> R.drawable.ic_launcher_background // TODO add placeholder image
                    }
                ), contentDescription = "current weather",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .size(20.dp)

            )
        }
        Column(
            modifier = Modifier
                .align(AbsoluteAlignment.CenterRight)
        ) {
            Text(
                text = AnnotatedString(
                    text = stringResource(
                        id = R.string.celsius,
                        data.averageDegrees.toInt().toString()
                    )
                ),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Center
            )
        }
    }
}