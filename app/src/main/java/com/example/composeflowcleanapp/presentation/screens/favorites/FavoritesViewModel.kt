package com.example.composeflowcleanapp.presentation.screens.favorites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.model.FavoriteData
import com.example.domain.usecase.AddToFavoritesUseCase
import com.example.domain.usecase.GetFavoritesUseCase
import com.example.domain.usecase.RemoveFromFavoritesUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class FavoritesViewModel(
    getFavoritesUseCase: GetFavoritesUseCase,
    private val addToFavoritesUseCase: AddToFavoritesUseCase,
    private val removeFromFavoritesUseCase: RemoveFromFavoritesUseCase
) : ViewModel() {

    val favoritesData: StateFlow<List<FavoriteData>> =
        getFavoritesUseCase().stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = emptyList()
        )


    fun removeLocationFromFavorites(item: FavoriteData) {
        viewModelScope.launch {
            removeFromFavoritesUseCase.invoke(item.latitude, item.longitude)
        }
    }

}