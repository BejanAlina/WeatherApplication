package com.example.composeflowcleanapp.presentation.navigation

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import com.example.composeflowcleanapp.R
import com.example.composeflowcleanapp.presentation.component.Background
import kotlinx.coroutines.launch


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun NavApp() {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    val navController = rememberNavController()
    Background {
        ModalNavigationDrawer(
            drawerState = drawerState,
            drawerContent = {
                ModalDrawerSheet {
                    Text(
                        stringResource(id = R.string.profile_zone),
                        modifier = Modifier.padding(16.dp)
                    )
                    HorizontalDivider()
                    NavigationDrawerItem(
                        label = { Text(text = stringResource(id = R.string.home_page_screen_title)) },
                        selected = false,
                        onClick = {
                            navController.navigateToMainScreen()
                            scope.launch { drawerState.close() }
                        }
                    )
                    NavigationDrawerItem(
                        label = { Text(text = stringResource(id = R.string.favorites_screen_title)) },
                        selected = false,
                        onClick = {
                            navController.navigateToFavoritesScreen()
                            scope.launch { drawerState.close() }
                        }
                    )
                }
            },
            content = {
                Scaffold(
                    topBar = {
                        IconButton(
                            modifier = Modifier.padding(top = 20.dp, start = 10.dp),
                            onClick = {
                                scope.launch {
                                    drawerState.apply {
                                        if (isClosed) open() else close()
                                    }
                                }
                            }) {
                            Icon(Icons.Filled.Menu, contentDescription = "")
                        }
                    }
                ) {
                    AppNavHost(navController)
                }
            }
        )
    }
}