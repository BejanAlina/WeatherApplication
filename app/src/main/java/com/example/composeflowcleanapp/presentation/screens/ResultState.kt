package com.example.composeflowcleanapp.presentation.screens

import com.example.domain.model.WeatherData

sealed interface ResultState {
    data object Loading : ResultState

    data object LoadFailed : ResultState

    data class Success(
        val data: List<WeatherData> = emptyList()
    ) : ResultState
}
