package com.example.composeflowcleanapp.presentation.navigation

import androidx.navigation.NavController
import androidx.navigation.NavOptions


const val MAIN_ROUTE = "main_route"
const val FAVORITES_ROUTE = "favorites_route"

fun NavController.navigateToMainScreen(navOptions: NavOptions? = null) =
    navigate(MAIN_ROUTE, navOptions)

fun NavController.navigateToFavoritesScreen(navOptions: NavOptions? = null) =
    navigate(FAVORITES_ROUTE, navOptions)