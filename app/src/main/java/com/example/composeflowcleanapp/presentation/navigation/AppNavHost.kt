package com.example.composeflowcleanapp.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.composeflowcleanapp.presentation.screens.favorites.FavoritesScreen
import com.example.composeflowcleanapp.presentation.screens.main.MainScreen

@Composable
fun AppNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    startDestination: String = MAIN_ROUTE,
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier,
    ) {
        composable(route = MAIN_ROUTE) {
            MainScreen()
        }

        composable(route = FAVORITES_ROUTE) {
            FavoritesScreen()
        }
    }
}
