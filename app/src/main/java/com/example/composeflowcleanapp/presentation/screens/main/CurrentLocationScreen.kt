import android.Manifest
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun CurrentLocationScreen(onLocationGranted: () -> Unit, onLocationDenied: () -> Unit) {
    val cameraPermissionState = rememberPermissionState(Manifest.permission.ACCESS_FINE_LOCATION)

    val requestPermissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            onLocationGranted.invoke()
        } else {
            onLocationDenied.invoke()
        }
    }

    LaunchedEffect(cameraPermissionState) {
        println("cameraPermissionState.status" +cameraPermissionState.status)
        if (!cameraPermissionState.status.isGranted && cameraPermissionState.status.shouldShowRationale) {
            onLocationDenied.invoke()
        } else {
            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }
}


sealed interface PermissionState {
    data object Success : PermissionState
    data object Error : PermissionState
    data object Loading : PermissionState
}

@Composable
internal fun LocationDeniedScreen(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(34.dp),
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            textAlign = TextAlign.Center,
            text = "This application requires location services in order to display the weather." +
                    "\n Please enable location from app settings"
        )
    }
}