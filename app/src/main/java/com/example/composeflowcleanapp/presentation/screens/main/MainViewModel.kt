package com.example.composeflowcleanapp.presentation.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.composeflowcleanapp.presentation.screens.Result
import com.example.composeflowcleanapp.presentation.screens.ResultState
import com.example.composeflowcleanapp.presentation.screens.asResult
import com.example.domain.usecase.AddToFavoritesUseCase
import com.example.domain.usecase.GetDataUseCase
import com.example.domain.usecase.IsFavoriteLocationUseCase
import com.example.domain.usecase.LocationTracker
import com.example.domain.usecase.RemoveFromFavoritesUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

@OptIn(ExperimentalCoroutinesApi::class)
class MainViewModel(
    private val locationUseCase: LocationTracker,
    private val getDataUseCase: GetDataUseCase,
    private val isLocationFavoriteUseCase: IsFavoriteLocationUseCase,
    private val addToFavoritesUseCase: AddToFavoritesUseCase,
    private val removeFromFavoritesUseCase: RemoveFromFavoritesUseCase
) : ViewModel() {
    private val currentLocation = MutableStateFlow<Pair<Double, Double>?>(null)

    val isInFavorite: StateFlow<Boolean?> =
        currentLocation.filterNotNull().flatMapLatest {
            isLocationFavoriteUseCase(it.first, it.second)
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = null,
        )

    val uiState: StateFlow<ResultState> =
        currentLocation.flatMapLatest {
            if (it == null) {
                return@flatMapLatest flowOf(ResultState.LoadFailed)
            }
            getDataUseCase(it.first, it.second).asResult().map { result ->
                when (result) {
                    is Result.Success -> ResultState.Success(result.data)
                    is Result.Loading -> ResultState.Loading
                    is Result.Error -> ResultState.LoadFailed
                }
            }
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = ResultState.Loading
        )

    fun getCurrentLocation() {
        viewModelScope.launch {
            try {
                currentLocation.value = locationUseCase.getCurrentLocation()
                println("my location is ${currentLocation.value}")
            } catch (exception: Exception) {
                currentLocation.emit(null)
            }
        }
    }

    fun addLocationToFavorites() {
        viewModelScope.launch {
            addToFavoritesUseCase.invoke(
                currentLocation.value?.first ?: 0.0,
                currentLocation.value?.second ?: 0.0
            )
        }
    }

    fun removeLocationFromFavorites() {
        viewModelScope.launch {
            removeFromFavoritesUseCase.invoke(
                currentLocation.value?.first ?: 0.0,
                currentLocation.value?.second ?: 0.0
            )
        }
    }

}