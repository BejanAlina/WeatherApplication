package com.example.composeflowcleanapp.presentation.screens.favorites

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.composeflowcleanapp.R
import org.koin.androidx.compose.koinViewModel

@Composable
fun FavoritesScreen(
    favoritesViewModel: FavoritesViewModel = koinViewModel()
) {
    val favoritesData by favoritesViewModel.favoritesData.collectAsStateWithLifecycle()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 100.dp)
    ) {

        val hasData = favoritesData.isNotEmpty()

        Text(
            modifier = Modifier.padding(12.dp),
            text = if (hasData) "My favorite locations" else "No Favorites"
        )
        if (hasData) {
            Spacer(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .background(Color.Black)
            )
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 40.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                itemsIndexed(favoritesData) { index, item ->
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(horizontal = 8.dp)
                    ) {
                        Text(
                            modifier = Modifier.weight(4f),
                            text = "Location ${index + 1} lat: ${item.latitude}  long : ${item.longitude}"
                        )
                        IconButton(
                            modifier = Modifier.weight(0.2f),
                            onClick = { favoritesViewModel.removeLocationFromFavorites(item) }) {
                            Icon(
                                imageVector = Icons.Filled.Delete,
                                contentDescription = stringResource(
                                    id = R.string.delete,
                                ),
                            )
                        }
                    }
                }
            }
        }
    }
}