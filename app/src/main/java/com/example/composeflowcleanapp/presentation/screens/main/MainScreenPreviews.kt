package com.example.composeflowcleanapp.presentation.screens.main

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.example.composeflowcleanapp.presentation.screens.ResultState
import com.example.data.repository.sdf
import com.example.domain.model.WeatherData
import com.example.domain.model.WeatherState

@Preview
@Composable
fun RainyPreview() {
    val list = listOf(
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-24 12:00:00")
        ),
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-25 12:00:00")
        ),
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-26 12:00:00")
        ),
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-27 12:00:00")
        ),
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-28 12:00:00")
        ),
        WeatherData(
            WeatherState.RAIN,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-29 12:00:00")
        )
    )
    val state = ResultState.Success(list)
    MainScreen(uiState = state, isFavorite = false)
}

@Preview
@Composable
fun SunnyPreview() {
    val list = listOf(
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-24 12:00:00")
        ),
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-25 12:00:00")
        ),
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-26 12:00:00")
        ),
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-27 12:00:00")
        ),
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-28 12:00:00")
        ),
        WeatherData(
            WeatherState.SUNNY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-29 12:00:00")
        )
    )
    val state = ResultState.Success(list)
    MainScreen(uiState = state, isFavorite = false)
}


@Preview
@Composable
fun CloudyPreview() {
    val list = listOf(
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-24 12:00:00")
        ),
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-25 12:00:00")
        ),
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-26 12:00:00")
        ),
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-27 12:00:00")
        ),
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-28 12:00:00")
        ),
        WeatherData(
            WeatherState.CLOUDY,
            minDegrees = 30.0,
            maxDegrees = 35.0,
            averageDegrees = 33.0,
            date = sdf.parse("2024-02-29 12:00:00")
        )
    )
    val state = ResultState.Success(list)
    MainScreen(uiState = state, isFavorite = false)
}