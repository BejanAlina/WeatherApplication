package com.example.composeflowcleanapp.di

import com.example.composeflowcleanapp.BuildConfig
import com.example.data.setup.AppServiceFactory
import com.example.data.setup.HttpClientFactory
import com.example.data.setup.ServiceFactory
import org.koin.core.qualifier.named
import org.koin.dsl.module

val remoteModule = module {
    single(named(HTTP_CLIENT)) {
        HttpClientFactory(
            BuildConfig.DEBUG
        )
    }
    single(named(SERVICE_FACTORY)) { ServiceFactory(BASE_URL) }
    single(named(APP_SERVICE)) {
        AppServiceFactory(get(named(HTTP_CLIENT)), BuildConfig.API_KEY).getAppService(
            get(
                named(
                    SERVICE_FACTORY
                )
            )
        )
    }
}