package com.example.composeflowcleanapp.di

import com.example.data.local.LocalDataSource
import com.example.data.local.database.AppDatabase
import com.example.data.local.database.DatabaseConstants
import com.example.data.remote.RemoteDataSource
import com.example.data.repository.UserDataRepository
import com.example.data.setup.DatabaseFactory
import com.example.domain.repository.Repository
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

val localModule = module {
    single(named(DatabaseConstants.DATABASE_NAME)) {
        DatabaseFactory().getOrCreateDatabaseInstance(androidApplication())
    }
    single { get<AppDatabase>().databaseDao() }
    single(named(LOCAL_DATA_SOURCE)) { LocalDataSource(get(named(DatabaseConstants.DATABASE_NAME))) }
    single(named(REMOTE_DATA_SOURCE)) { RemoteDataSource(get(named(APP_SERVICE))) }
    single<Repository>(named(REPO)) {
        UserDataRepository(
            remoteDataSource = get(named(REMOTE_DATA_SOURCE)),
            localDataSource = get(named(LOCAL_DATA_SOURCE))
        )
    }
}