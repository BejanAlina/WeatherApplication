package com.example.composeflowcleanapp.di

import com.example.composeflowcleanapp.presentation.screens.favorites.FavoritesViewModel
import com.example.composeflowcleanapp.presentation.screens.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        MainViewModel(
            get(named(GET_LOCATION_USE_CASE)),
            get(named(GET_DATA_USE_CASE)),
            get(named(IS_LOCATION_FAVORITE_USE_CASE)),
            get(named(ADD_TO_FAVORITES_USE_CASE)),
            get(named(REMOVE_FROM_FAVORITES_USE_CASE))
        )
    }
    viewModel {
        FavoritesViewModel(
            get(named(GET_FAVORITES_USE_CASE)),
            get(named(ADD_TO_FAVORITES_USE_CASE)),
            get(named(REMOVE_FROM_FAVORITES_USE_CASE))
        )
    }
}
