package com.example.composeflowcleanapp.di

import com.example.domain.usecase.AddToFavoritesUseCase
import com.example.domain.usecase.GetDataUseCase
import com.example.domain.usecase.GetFavoritesUseCase
import com.example.domain.usecase.GetLocationUseCase
import com.example.domain.usecase.IsFavoriteLocationUseCase
import com.example.domain.usecase.LocationTracker
import com.example.domain.usecase.RemoveFromFavoritesUseCase
import com.google.android.gms.location.LocationServices
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

val domainModule = module {
    factory(named(GET_DATA_USE_CASE)) { GetDataUseCase(get(named(REPO))) }
    factory(named(IS_LOCATION_FAVORITE_USE_CASE)) { IsFavoriteLocationUseCase(get(named(REPO))) }
    factory(named(ADD_TO_FAVORITES_USE_CASE)) { AddToFavoritesUseCase(get(named(REPO))) }
    factory(named(REMOVE_FROM_FAVORITES_USE_CASE)) { RemoveFromFavoritesUseCase(get(named(REPO))) }
    factory(named(GET_FAVORITES_USE_CASE)) { GetFavoritesUseCase(get(named(REPO))) }
    factory<LocationTracker>(named(GET_LOCATION_USE_CASE)) {
        GetLocationUseCase(
            LocationServices.getFusedLocationProviderClient(
                androidApplication()
            )
        )
    }
}
