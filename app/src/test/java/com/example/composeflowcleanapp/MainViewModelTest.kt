package com.example.composeflowcleanapp

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.composeflowcleanapp.presentation.screens.ResultState
import com.example.composeflowcleanapp.presentation.screens.main.MainViewModel
import com.example.data.dtos.MainDTO
import com.example.data.dtos.WeatherDTO
import com.example.data.dtos.WeatherListDTO
import com.example.data.dtos.WeatherResponseDTO
import com.example.data.local.LocalDataSource
import com.example.data.local.database.AppDatabase
import com.example.data.remote.AppService
import com.example.data.remote.RemoteDataSource
import com.example.data.repository.UserDataRepository
import com.example.data.repository.sdf
import com.example.domain.model.WeatherData
import com.example.domain.model.WeatherState
import com.example.domain.usecase.AddToFavoritesUseCase
import com.example.domain.usecase.GetDataUseCase
import com.example.domain.usecase.IsFavoriteLocationUseCase
import com.example.domain.usecase.LocationTracker
import com.example.domain.usecase.RemoveFromFavoritesUseCase
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Response

@RunWith(AndroidJUnit4::class)
class MainViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val mockAppService = mock<AppService>()
    private val mockDataBase = mock<AppDatabase>()
    private val localDataSource = LocalDataSource(mockDataBase)
    private val remoteDataSource = RemoteDataSource(mockAppService)
    private val repository = UserDataRepository(remoteDataSource, localDataSource)
    private val getLocationUseCase = mock<LocationTracker>()
    private val getDataUseCase = GetDataUseCase(repository)
    private val isLocationFavoriteUseCase = IsFavoriteLocationUseCase(repository)
    private val addToFavoritesUseCase = AddToFavoritesUseCase(repository)
    private val removeFromFavoritesUseCase = RemoveFromFavoritesUseCase(repository)

    private val viewModel = MainViewModel(
        getLocationUseCase,
        getDataUseCase,
        isLocationFavoriteUseCase,
        addToFavoritesUseCase,
        removeFromFavoritesUseCase
    )

    @Test
    fun viewModelSuccessResult() = runTest {
        val lat = 1.0
        val long = 2.0
        val list = listOf(
            WeatherListDTO(
                main = MainDTO(280.4, 340.2, 220.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(280.4, 340.2, 220.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(280.4, 340.2, 220.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(280.4, 240.2, 220.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(280.4, 240.2, 220.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:40:00"
            )
        )
        whenever(mockAppService.getWeather(lat, long)) doReturn Response.success(
            WeatherResponseDTO(
                list
            )
        )

        whenever(getLocationUseCase.getCurrentLocation()) doReturn Pair(1.0, 2.0)

        viewModel.getCurrentLocation()

        val collectJob = launch(UnconfinedTestDispatcher()) { viewModel.uiState.collect() }

        val expectedResult =
            listOf(
                WeatherData(
                    WeatherState.fromString("Rain"),
                    7.0,
                    67.00,
                    37.0,
                    sdf.parse("2024-02-20 12:00:00")
                )
            )
        assertEquals(ResultState.Success(expectedResult), viewModel.uiState.value)
        collectJob.cancel()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun viewModelFailResult() = runTest {
        val lat = 1.0
        val long = 2.0
        val list = listOf(
            WeatherListDTO(
                main = MainDTO(100.4, 140.2, 120.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:00:00"
            ),
            WeatherListDTO(
                main = MainDTO(100.4, 140.2, 120.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:10:00"
            ),
            WeatherListDTO(
                main = MainDTO(100.4, 140.2, 120.0),
                weather = listOf(WeatherDTO("Rain")),
                dateText = "2024-02-20 12:20:00"
            ),
            WeatherListDTO(
                main = MainDTO(100.4, 140.2, 120.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:30:00"
            ),
            WeatherListDTO(
                main = MainDTO(100.4, 140.2, 120.0),
                weather = listOf(WeatherDTO("Clouds")),
                dateText = "2024-02-20 12:40:00"
            )
        )
        whenever(mockAppService.getWeather(lat, long)) doReturn Response.success(
            WeatherResponseDTO(
                list
            )
        )

        whenever(getLocationUseCase.getCurrentLocation()) doThrow RuntimeException()

        viewModel.getCurrentLocation()

        val collectJob = launch(UnconfinedTestDispatcher()) { viewModel.uiState.collect() }

        assertEquals(ResultState.LoadFailed, viewModel.uiState.value)
        collectJob.cancel()
    }
}