import java.util.Properties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
}

android {
    namespace = "com.example.composeflowcleanapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.composeflowcleanapp"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        val properties = Properties()
        properties.load(project.rootProject.file("local.properties").inputStream())
        buildConfigField("String", "API_KEY", properties.getProperty("API_KEY"))
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        buildConfig = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.7"
    }
}

dependencies {
    implementation("androidx.core:core-splashscreen:${Versions.SPLASH}")
    implementation("androidx.core:core-ktx:${Versions.KTX}")
    implementation("com.google.android.material:material:${Versions.MATERIAL}")
    implementation("androidx.appcompat:appcompat:${Versions.COMPAT}")
    implementation("androidx.compose.material3:material3-window-size-class-android:${Versions.WSCA}")
    implementation("com.google.android.gms:play-services-location:${Versions.GMS}")
    implementation("com.google.accompanist:accompanist-permissions:${Versions.ACMP}")
    implementation("androidx.compose.ui:ui-tooling-preview-android:${Versions.COMPOSE_PREVIEW}")

    val composeBom = platform("androidx.compose:compose-bom:${Versions.BOM}")
    implementation(composeBom)
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.foundation:foundation")
    implementation("androidx.compose.material3:material3")
    debugImplementation("androidx.compose.ui:ui-tooling:${Versions.COMPOSE_UI_TOOLING}")
    implementation("androidx.lifecycle:lifecycle-runtime-compose:${Versions.COMPOSE_LIFECYCLE}")
    implementation("androidx.navigation:navigation-runtime-ktx:${Versions.COMPOSE_NAVIGATION}")
    implementation("androidx.navigation:navigation-compose:${Versions.COMPOSE_NAVIGATION}")
    implementation("androidx.activity:activity-compose:${Versions.COMPOSE_ACTIVITY}")

    implementation("io.insert-koin:koin-core:${Versions.KOIN}")
    implementation("io.insert-koin:koin-android:${Versions.KOIN}")
    implementation("io.insert-koin:koin-androidx-navigation:${Versions.KOIN}")
    implementation("io.insert-koin:koin-androidx-compose:${Versions.KOIN}")
    kapt("androidx.room:room-compiler:${Versions.ROOM}")

    implementation(project(":domain"))
    implementation(project(":data"))

    testImplementation("junit:junit:${Versions.JUNIT}")
    testImplementation("androidx.test.ext:junit-ktx:${Versions.JUNIT_KTX}")
    testImplementation("org.robolectric:robolectric:${Versions.ROBOELECTRIC}")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.MOCK}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.COROUTINE_TEST}")
    testImplementation(kotlin("test"))
}