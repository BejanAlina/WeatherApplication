/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Versions {
    const val KTX = "1.12.0"
    const val COMPAT = "1.6.1"
    const val SPLASH = "1.0.1"
    const val WSCA = "1.2.0"
    const val BOM = "2024.02.00"
    const val OK_HTTP = "4.12.0"
    const val RETROFIT = "2.9.0"
    const val COMPOSE_LIFECYCLE = "2.7.0"
    const val COMPOSE_NAVIGATION = "2.7.7"
    const val COMPOSE_ACTIVITY = "1.8.2"
    const val COMPOSE_PREVIEW = "1.6.2"
    const val COMPOSE_UI_TOOLING = "1.6.2"
    const val GMS = "21.1.0"
    const val ACMP = "0.35.0-alpha"
    const val MATERIAL = "1.11.0"
    const val ROOM = "2.6.1"
    const val KOIN = "3.4.3"
    const val JUNIT = "4.13.2"
    const val JUNIT_KTX = "1.1.5"
    const val ROBOELECTRIC = "4.11.1"
    const val MOCK = "2.2.0"
    const val COROUTINE_TEST = "1.8.0"

}